import queue
from log import logger as my_logger
from controller.path_controller import PathController
from api.uart_thread import UartThread


if __name__ == "__main__":
    logger = my_logger.setup_custom_logger('PATH')

    q = queue.Queue()
    path = PathController(q)
    api = UartThread(q)

    api.start()

    try:
        path.app.exec_()
    except Exception as e:
        logger.error('ERROR: {}'.format(str(e)))
        pass

    path.close()
    api.join()
    api.close()
    q.join()

