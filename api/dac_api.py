import logging
logger = logging.getLogger('PATH')

dac_mapping = {
    'EXT-1': 1,
    'EXT-2': 2,
    'EXT-3': 3,
    'EXT-4': 4,
    'EXT-5': 5,
    'EXT-6': 6,
    'EXT-7': 7,
    'EXT-8': 8,
    'EXT-9': 9,
    'EXT-10': 10,
    'INT-SUPPLY-ISET': 11,
    'INT-SUPPLY-VSET': 12,
    'INT-LOAD-ISET': 13,
}

MODULE = 5

COMMANDS = {
    'SET_RAW': 0,
}

# private

class DacData:

    def __init__(self, channel):

        self.channel = channel
        self.raw_value = 0
        self.set = False


dac_data_store = {}


def dac_set_raw_resp(message):
    channel = message[1]
    value = int(((message[2] & 0b11) << 8) | message[3])
    valid = message[4]
    logger.debug('dac parse set raw response: valid={}, channel={}, value={}'.format(valid, channel, value))
    if valid:
        dac_data_store[channel].raw_value = value
        dac_data_store[channel].set = True


# public


def dac_set_raw(dac_channel, value):
    if dac_channel not in dac_mapping:
        logger.error('dac channel {} not in mapping'.format(dac_channel))
        return

    dac_data_store[dac_mapping[dac_channel]] = DacData(dac_channel)

    # limit value to 10 bit
    value = int(value)
    if value > 1023:
        value = 1023
    if value < 0:
        value = 0

    logger.debug('dac set raw {} to {}'.format(dac_channel, value))

    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['SET_RAW'] & 0xF)
    message[1] = dac_mapping[dac_channel]
    message[2] = (value >> 8 & 0b11)
    message[3] = (value & 0xFF)
    message[7] = 0x17
    return message


def dac_get_set_status(dac_channel):
    if dac_channel not in dac_mapping:
        logger.error('dac channel {} not in mapping'.format(dac_channel))
        return False

    return dac_data_store[dac_mapping[dac_channel]].set


def dac_parse_resp(command, message):
    logger.debug('dac parse response')
    if command == COMMANDS['SET_RAW']:
        dac_set_raw_resp(message)


