import logging
logger = logging.getLogger('PATH')

MODULE = 8

COMMANDS = {
    'ENABLE': 0,
    'ISENSE': 1,
}


fixed_mapping = {
    '5V': 0,
    '12V': 1,
    '24V': 2,
    'DATA': 3,
}

# private


def fixed_enable_resp(message):
    channel = message[1]
    state = message[2]
    valid = message[3]

    channel_name = None
    for m in fixed_mapping:
        if fixed_mapping[m] == channel:
            channel_name = m
    state_name = 'ENABLED' if state == 1 else 'DISABLED'

    logger.debug('load parse load_enable_resp response: valid={}, channel={}, state={}'.format(valid, channel_name if channel_name else channel, state_name))


def fixed_get_current_resp(message):
    channel = message[1]
    value = int(((message[2] & 0xF) << 8) | message[3])
    valid = message[4]

    channel_name = None
    for m in fixed_mapping:
        if fixed_mapping[m] == channel:
            channel_name = m

    logger.debug('load parse load_set_current_resp response: valid={}, channel={}, value={}'.format(valid, channel_name if channel_name else channel, value))


# public


def fixed_enable(channel):

    if channel not in fixed_mapping:
        logger.error("ERROR: channel {} not in mapping".format(channel))
        return bytearray(8)

    logger.debug('fixed enable channel {}'.format(fixed_mapping[channel]))
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ENABLE'] & 0xF)
    message[1] = fixed_mapping[channel]
    message[2] = 1
    message[7] = 0x17

    data_enable_message = bytearray(8)
    data_enable_message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ENABLE'] & 0xF)
    data_enable_message[1] = fixed_mapping['DATA']
    data_enable_message[2] = 1
    data_enable_message[7] = 0x17

    return [data_enable_message, message]


def fixed_disable(channel):

    if channel not in fixed_mapping:
        logger.error("ERROR: channel {} not in mapping".format(channel))
        return bytearray(8)

    logger.debug('fixed disable channel {}'.format(fixed_mapping[channel]))
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ENABLE'] & 0xF)
    message[1] = fixed_mapping[channel]
    message[2] = 0
    message[7] = 0x17

    return message


def fixed_get_current(channel):
    if channel not in fixed_mapping:
        logger.error("ERROR: channel {} not in mapping".format(channel))
        return bytearray(8)

    logger.debug('fixed get current for channel {}'.format(fixed_mapping[channel]))
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ISENSE'] & 0xF)
    message[1] = fixed_mapping[channel]
    message[7] = 0x17
    return message


def fixed_parse_resp(command, message):
    logger.debug('fixed parse response')
    if command == COMMANDS['ENABLE']:
        fixed_enable_resp(message)
    elif command == COMMANDS['ISENSE']:
        fixed_get_current_resp(message)

