import serial, logging, threading, queue, time, sys, glob
from .adc_api import adc_parse_resp
from .dio_api import dio_parse_resp
from .can_api import can_parse_resp
from .spi_api import spi_parse_resp
from .dac_api import dac_parse_resp
from .supply_api import supply_parse_resp
from .load_api import load_parse_resp
from .fixed_api import fixed_parse_resp
logger = logging.getLogger('PATH')


MODULES = {
    'MICRO': {
        'ID': 0
    },
    'ADC': {
        'ID': 1,
        'COMMANDS': {
            'INIT': 0,
            'READ': 1,
        }
    },
    'DIO': {
        'ID': 2
    },
    'CAN': {
        'ID': 3
    },
    'SPI': {
        'ID': 4
    },
    'DAC': {
        'ID': 5
    },
    'SUPPLY': {
        'ID': 6
    },
    'LOAD': {
        'ID': 7
    },
    'FIXED': {
        'ID': 8
    },
    'LAST': 9
}


def uart_message(module, command, *data):
    if module in MODULES:
        if command in MODULES[module]['COMMANDS']:
            module_command = ((MODULES[module]['ID'] & 0xF) << 4) + (MODULES[module]['COMMANDS'][command] & 0xF)
            return bytearray([module_command])+bytearray(data)+bytearray(6-len(data))+bytearray([23])
    return None


def get_module(message):
    return (message[0] & 0xF0) >> 4


def get_command(message):
    return message[0] & 0xF


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


class UartThread(threading.Thread):

    def __init__(self, client):
        super().__init__()
        logger.debug('init')

        self.client = client
        try:
            port = serial_ports()[0]
            self.uart = serial.Serial(port, 115200, timeout=0.0005)
            logger.debug('Found serial port for UART bridge device {}'.format(port))
        except Exception as e:
            logger.error(str(e))
            pass

    def run(self):
        logger.debug('running')
        if hasattr(self, 'uart'):
            while True:
                # tx
                try:
                    message = self.client.get(block=False)
                    if message:
                        if type(message) == bytearray:
                            logger.debug('uart tx: {}'.format(str(message)))
                            self.uart.write(message)
                        if type(message) == list:
                            m = message.pop(0) # take one message out
                            logger.debug('uart tx: {}'.format(str(m)))
                            self.uart.write(m) # send it
                            self.client.put(message) # add rest of list back

                except queue.Empty:
                    pass

                # rx
                try:
                    message = bytearray(self.uart.read(9))
                    if len(message) == 8:
                        logger.debug('uart rx: {}'.format(message))
                        module = get_module(message)
                        if module < MODULES['LAST']:
                            if module == MODULES['ADC']['ID']:
                                adc_parse_resp(get_command(message), message)
                            elif module == MODULES['DIO']['ID']:
                                dio_parse_resp(get_command(message), message)
                            elif module == MODULES['CAN']['ID']:
                                can_parse_resp(get_command(message), message)
                            elif module == MODULES['SPI']['ID']:
                                spi_parse_resp(get_command(message), message)
                            elif module == MODULES['DAC']['ID']:
                                dac_parse_resp(get_command(message), message)
                            elif module == MODULES['SUPPLY']['ID']:
                                supply_parse_resp(get_command(message), message)
                            elif module == MODULES['LOAD']['ID']:
                                load_parse_resp(get_command(message), message)
                            elif module == MODULES['FIXED']['ID']:
                                fixed_parse_resp(get_command(message), message)

                except serial.SerialException:
                    pass

                time.sleep(0.0005)

    def close(self):
        logger.debug('close')
        if hasattr(self, 'uart'):
            self.uart.close()

    def send_test(self):
        if hasattr(self, 'uart'):
            self.uart.write(b'Hello')
