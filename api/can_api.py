import logging
import cantools
logger = logging.getLogger('PATH')

MODULE = 3

COMMANDS = {
    'INIT': 0,
    'WRITE_ONCE': 1,
    'WRITE_FREQUENCY': 2,
    'RECEIVE_MESSAGE': 3,
    'MORE': 4,
}


class CanData:

    def __init__(self, baud_rate_index=None):

        self.baud_rate_index = baud_rate_index
        self.init = False
        self.rx_messages = {}
        self.tx_messages = {}


# global data


can_data = CanData()
write_once_current_message_id = None
write_frequency_current_message_id = None
receive_message_current_id = None
receive_message_current_data = None

# private


def can_init_resp(message):
    baud_rate_index = message[1]
    valid = message[2]
    if valid and baud_rate_index == can_data.baud_rate_index:
        can_data.init = True
    logger.debug('can parse init response: valid={}, baud_rate={}'.format(valid, baud_rate_index))


def can_write_once_resp(message, more=False):
    global write_once_current_message_id
    valid = message[3]
    if valid:
        if not more:
            write_once_current_message_id = message[0] << 8 | message[1]
        elif  write_once_current_message_id:
            if write_once_current_message_id == (message[0] << 8 | message[1]):
                can_data.tx_messages[write_once_current_message_id]['count'] += 1
    logger.debug('can parse write once response: valid={}, more={}, message_id='.format(valid, more, write_once_current_message_id))


def can_write_frequency_resp(message, more=False):
    global write_frequency_current_message_id   # need this to store id temporarily while we wait for more message
    valid = message[3]
    if valid:
        if not more:
            write_frequency_current_message_id = message[1] << 8 | message[2]
        elif write_frequency_current_message_id:
            frequency = message[1] << 8 | message[2]
            if can_data.tx_messages[write_frequency_current_message_id]['frequency'] == frequency:
                can_data.tx_messages[write_frequency_current_message_id]['count'] += 1
            write_frequency_current_message_id = None
    logger.debug('can parse write frequency response: valid={}, more={}, message_id={}'.format(valid, more, write_frequency_current_message_id))


def can_receive_message_resp(message, more=False):
    global receive_message_current_id, receive_message_current_data
    message_id = message[1] << 8 | message[2]

    if receive_message_current_id and receive_message_current_data and receive_message_current_id == message_id:     # received other message
        if more:
            receive_message_current_data = bytearray(
                [message[3], message[4], message[5], message[6]]) + receive_message_current_data
        else:
            receive_message_current_data = receive_message_current_data + bytearray(
                [message[3], message[4], message[5], message[6]])

        if message_id not in can_data.rx_messages:  # if dont have message in rx bc not in db or no db
            can_data.rx_messages[message_id] = cantools.db.Message(receive_message_current_id, "", 8, [])
        if 'data' in can_data.rx_messages[message_id]:
            can_data.rx_messages[message_id]['data'] = receive_message_current_data
        else:
            can_data.rx_messages[message_id]['data'] = receive_message_current_data

        if 'count' in can_data.rx_messages[message_id]:
            can_data.rx_messages[message_id]['count'] += 1
        else:
            can_data.rx_messages[message_id]['count'] = 1

        receive_message_current_id = None
        receive_message_current_data = None
    else:
        receive_message_current_id = message_id
        receive_message_current_data = bytearray([message[3], message[4], message[5], message[6]])

    logger.debug('can parse receive message response: message_id={}, more={}'.format(message_id, more))


# public


def can_init(baud_rate_index):
    logger.debug('can init')
    can_data.baud_rate_index = baud_rate_index
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['INIT'] & 0xF)
    message[1] = baud_rate_index
    message[7] = 0x17
    return message


def can_get_init():
    logger.debug('can get init')
    return can_data.init


def can_write_message_once(frame_id, frame_data):
    logger.debug('can write message once')

    if frame_id not in can_data.tx_messages:
        can_data.tx_messages[frame_id] = {}
    can_data.tx_messages[frame_id]['data'] = frame_data
    can_data.tx_messages[frame_id]['count'] = 0
    can_data.tx_messages[frame_id]['frequency'] = 0  # no frequency since written once

    if frame_id > 0xFFFF:
        logger.error('Message ID too big')
        return None

    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['WRITE_ONCE'] & 0xF)
    message[1] = (frame_id >> 8) & 0xFF
    message[2] = frame_id & 0xFF
    message[3] = frame_data[3]
    message[4] = frame_data[2]
    message[5] = frame_data[1]
    message[6] = frame_data[0]
    message[7] = 0x17

    more = bytearray(8)
    more[0] = ((MODULE & 0xF) << 4) | ((COMMANDS['WRITE_ONCE'] + COMMANDS['MORE']) & 0xF)
    more[1] = 0
    more[2] = 0
    more[3] = frame_data[7]
    more[4] = frame_data[6]
    more[5] = frame_data[5]
    more[6] = frame_data[4]
    more[7] = 0x17

    return [message, more]


def can_write_message_frequency(frame_id, frame_data, frequency):
    logger.debug('can write message at {} Hz'.format(frequency))

    if frame_id not in can_data.tx_messages:
        can_data.tx_messages[frame_id] = {}
    can_data.tx_messages[frame_id]['data'] = frame_data
    can_data.tx_messages[frame_id]['count'] = 0
    can_data.tx_messages[frame_id]['frequency'] = frequency

    if frame_id > 0xFFFF:
        logger.error('Message ID too big')
        return None

    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['WRITE_FREQUENCY'] & 0xF)
    message[1] = (frame_id >> 8) & 0xFF
    message[2] = frame_id & 0xFF
    message[3] = frame_data[3]
    message[4] = frame_data[2]
    message[5] = frame_data[1]
    message[6] = frame_data[0]
    message[7] = 0x17

    more = bytearray(8)
    more[0] = ((MODULE & 0xF) << 4) | ((COMMANDS['WRITE_FREQUENCY'] + COMMANDS['MORE']) & 0xF)
    more[1] = (frequency >> 8) & 0xFF
    more[2] = frequency & 0xFF
    more[3] = frame_data[7]
    more[4] = frame_data[6]
    more[5] = frame_data[5]
    more[6] = frame_data[4]
    more[7] = 0x17

    return [message, more]


def can_get_rx_messages():
    return can_data.rx_messages


def can_get_tx_messages():
    return can_data.tx_messages


def can_parse_resp(command, message):
    logger.debug('can parse response')
    if command == COMMANDS['INIT']:
        can_init_resp(message)
    elif command == COMMANDS['WRITE_ONCE']:
        can_write_once_resp(message)
    elif command == COMMANDS['WRITE_FREQUENCY']:
        can_write_frequency_resp(message)
    elif command == COMMANDS['RECEIVE_MESSAGE']:
        can_receive_message_resp(message)
    elif command == COMMANDS['WRITE_ONCE']+COMMANDS['MORE']:
        can_write_once_resp(message, more=True)
    elif command == COMMANDS['WRITE_FREQUENCY']+COMMANDS['MORE']:
        can_write_frequency_resp(message, more=True)
    elif command == COMMANDS['RECEIVE_MESSAGE']+COMMANDS['MORE']:
        can_receive_message_resp(message, more=True)
