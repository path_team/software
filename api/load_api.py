import logging
logger = logging.getLogger('PATH')

MODULE = 7

COMMANDS = {
    'ENABLE': 0,
    'ISET': 1,
    'ISENSE': 2,
    'VSENSE_INPUT': 3,
    'VSENSE_INTER': 4,
    'TSENSE': 5,
}

# private


def load_enable_resp(message):
    valid = message[2]
    logger.debug('load parse load_enable_resp response: valid={}, '.format(valid))


def load_set_current_resp(message):
    valid = message[3]
    logger.debug('load parse load_set_current_resp response: valid={}, '.format(valid))


def load_get_current_resp(message):
    valid = message[3]
    value = int(((message[1] & 0xF) << 8) | message[2])
    logger.debug('load parse load_get_current_resp response: valid={}, value={}'.format(valid, value))


def load_get_input_voltage_resp(message):
    valid = message[3]
    value = int(((message[1] & 0xF) << 8) | message[2])
    logger.debug('load parse load_get_input_voltage_resp response: valid={}, value={}'.format(valid, value))


def load_get_inter_voltage_resp(message):
    valid = message[3]
    value = int(((message[1] & 0xF) << 8) | message[2])
    logger.debug('load parse load_get_inter_voltage_resp response: valid={}, value={}'.format(valid, value))


def load_get_temperature_resp(message):
    valid = message[3]
    value = int(((message[1] & 0xF) << 8) | message[2])
    logger.debug('load parse load_get_temperature_resp response: valid={}, value={}'.format(valid, value))


# public


def load_enable():
    logger.debug('load enable')
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ENABLE'] & 0xF)
    message[1] = 1
    message[7] = 0x17
    return message


def load_set_current(value):
    logger.debug('load set current {}'.format(value))
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ISET'] & 0xF)
    message[1] = (value >> 8 & 0b11)
    message[2] = (value & 0xFF)
    message[7] = 0x17
    return message


def load_get_current():
    logger.debug('load get current')
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ISENSE'] & 0xF)
    message[7] = 0x17
    return message


def load_get_input_voltage():
    logger.debug('load get input voltage')
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['VSENSE_INPUT'] & 0xF)
    message[7] = 0x17
    return message


def load_get_inter_voltage():
    logger.debug('load get inter voltage')
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['VSENSE_INTER'] & 0xF)
    message[7] = 0x17
    return message


def load_get_temperature():
    logger.debug('load get temperature')
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['TSENSE'] & 0xF)
    message[7] = 0x17
    return message


def load_parse_resp(command, message):
    logger.debug('load parse response')
    if command == COMMANDS['ENABLE']:
        load_enable_resp(message)
    elif command == COMMANDS['ISET']:
        load_set_current_resp(message)
    elif command == COMMANDS['ISENSE']:
        load_get_current_resp(message)
    elif command == COMMANDS['VSENSE_INPUT']:
        load_get_input_voltage_resp(message)
    elif command == COMMANDS['VSENSE_INTER']:
        load_get_inter_voltage_resp(message)
    elif command == COMMANDS['TSENSE']:
        load_get_temperature_resp(message)


