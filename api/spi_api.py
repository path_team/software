import logging
import cantools
logger = logging.getLogger('PATH')

MODULE = 4

COMMANDS = {
    'INIT': 0,
    'WRITE': 1,
    'READ': 2,
}

MAX_LENGTH = 5


class SpiData:

    def __init__(self, response_data=[]):

        self.init = False
        self.response_data = response_data
        self.response_ready = False

# global data


spi_data = SpiData()

# private


def spi_init_resp(message):
    valid = message[1]
    if valid:
        spi_data.init = True
    logger.debug('spi_init_resp, valid={}'.format(valid))


def spi_write_resp(message):
    valid = message[1]
    logger.debug('spi_write_resp, valid={}'.format(valid))


def spi_read_resp(message):
    bytes_left = message[1]

    for i in range(0, min(bytes_left, MAX_LENGTH)):
        spi_data.response_data.append(message[2+i])

    if bytes_left < MAX_LENGTH:
        spi_data.response_ready = True

    logger.debug('spi_read_resp, bytes_left={}'.format(bytes_left))

# public


def spi_init():
    logger.debug('spi_init')

    spi_data.init = False

    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['INIT'] & 0xF)
    message[7] = 0x17
    return message


def spi_get_init():
    logger.debug('spi_get_init')
    return spi_data.init


def spi_write(data, length):
    logger.debug('spi_write data={} length={}'.format(data, length))

    spi_data.response_data = []
    spi_data.response_ready = False

    messages = []
    for i in range(int(length/MAX_LENGTH)+1):
        message = bytearray(8)
        message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['WRITE'] & 0xF)
        message[1] = length
        for j in range(min(length, MAX_LENGTH)):
            message[2+j] = data[i*MAX_LENGTH+j]
        length -= min(length, MAX_LENGTH)
        message[7] = 0x17
        messages.append(message)

    return messages


def spi_get_response_data():
    if spi_data.response_ready:
        return spi_data.response_data
    return False


def spi_parse_resp(command, message):
    logger.debug('spi_parse_resp')
    if command == COMMANDS['INIT']:
        spi_init_resp(message)
    elif command == COMMANDS['WRITE']:
        spi_write_resp(message)
    elif command == COMMANDS['READ']:
        spi_read_resp(message)
