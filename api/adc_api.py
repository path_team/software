import logging
logger = logging.getLogger('PATH')

external_adc_mapping = {
    1: {
        'module': 0,
        'channel': 3,
    },
    2: {
        'module': 0,
        'channel': 1,
    },
    3: {
        'module': 0,
        'channel': 6,
    },
    4: {
        'module': 0,
        'channel': 11,
    },
    5: {
        'module': 0,
        'channel': 12,
    },
    6: {
        'module': 0,
        'channel': 14,
    },
    7: {
        'module': 1,
        'channel': 0,
    },
    8: {
        'module': 1,
        'channel': 7,
    },
    9: {
        'module': 1,
        'channel': 2,
    },
    10: {
        'module': 1,
        'channel': 8,
    }
}


def update_adc_mapping(custom_pin_mapping):
    """
    Used to change the pin mappings for special cases in external/test scripts
    """
    global external_adc_mapping
    external_adc_mapping = custom_pin_mapping


MODULE = 1

COMMANDS = {
    'INIT': 0,
    'READ': 1,
}


class AdcData:

    def __init__(self, module, channel):

        self.module = module
        self.channel = channel
        self.init = False
        self.raw_value = 0


adc_data_store = {}

# private


def adc_key(module, channel):
    return str(module)+'_'+str(channel)


def adc_init_resp(message):
    module = message[1]
    channel = message[2]
    valid = message[3]
    logger.debug('adc parse init response: valid={}, module={}, channel={}'.format(valid, module, channel))
    if valid:
        adc_data_store[adc_key(module, channel)].init = True


def adc_read_resp(message):
    module = message[1]
    channel = message[2]
    valid = message[3]
    data_h = message[4]
    data_l = message[5]
    value = (data_h << 8) | data_l
    logger.debug('adc parse read response: valid={}, module={}, channel={}, value={}'.format(valid, module, channel, value))
    if valid:
        adc_data_store[adc_key(module, channel)].raw_value = value

# public


def adc_init(adc_item_id):
    logger.debug('adc init {}'.format(adc_item_id))
    module = external_adc_mapping[adc_item_id]['module']
    channel = external_adc_mapping[adc_item_id]['channel']
    adc_data_store[adc_key(module, channel)] = AdcData(module, channel)
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['INIT'] & 0xF)
    message[1] = module
    message[2] = channel
    message[7] = 0x17
    return message


def adc_read(adc_item_id):
    logger.debug('adc read {}'.format(adc_item_id))
    module = external_adc_mapping[adc_item_id]['module']
    channel = external_adc_mapping[adc_item_id]['channel']

    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['READ'] & 0xF)
    message[1] = module
    message[2] = channel
    message[7] = 0x17
    return message


def adc_get_init(adc_item_id):
    # logger.debug('adc get init {}'.format(adc_item_id))
    module = external_adc_mapping[adc_item_id]['module']
    channel = external_adc_mapping[adc_item_id]['channel']
    return adc_data_store[adc_key(module, channel)].init


def adc_get_data(adc_item_id):
    # logger.debug('adc get data {}'.format(adc_item_id))
    module = external_adc_mapping[adc_item_id]['module']
    channel = external_adc_mapping[adc_item_id]['channel']
    return adc_data_store[adc_key(module, channel)].raw_value


def adc_parse_resp(command, message):
    logger.debug('adc parse response')
    if command == COMMANDS['INIT']:
        adc_init_resp(message)
    elif command == COMMANDS['READ']:
        adc_read_resp(message)


