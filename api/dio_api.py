import logging
logger = logging.getLogger('PATH')

external_dio_mapping = {
    1: {
        'channel': 4, #0
    },
    2: {
        'channel': 107,
    },
    3: {
        'channel': 106,
    },
    4: {
        'channel': 105,
    },
    5: {
        'channel': 44,
    },
    6: {
        'channel': 103,
    },
    7: {
        'channel': 43,
    },
    8: {
        'channel': 59,
    },
    9: {
        'channel': 58,
    },
    10: {
        'channel': 1,
    }
}


def update_dio_mapping(custom_pin_mapping):
    """
    Used to change the pin mappings for special cases in external/test scripts
    """
    global external_dio_mapping
    external_dio_mapping = custom_pin_mapping


MODULE = 2

COMMANDS = {
    'INIT': 0,
    'READ': 1,
    'WRITE': 2,
}


class DioData:

    def __init__(self, channel, setting):

        self.channel = channel
        self.setting = setting  # True is Input, False is Output
        self.init = False
        self.raw_value = False


dio_data_store = {}

# private


def dio_key(channel):
    return str(channel)


def dio_init_resp(message):
    channel = message[1]
    setting = message[2]
    valid = message[3]
    logger.debug('dio parse init response: valid={}, {}, channel={}'.format(valid, 'input' if setting == 1 else 'output', channel))
    if valid:
        dio_data_store[dio_key(channel)].init = True


def dio_read_resp(message):
    channel = message[1]
    value = message[2]
    valid = message[3]
    logger.debug('dio parse read response: valid={}, channel={}, value={}'.format(valid, channel, value))
    if valid:
        dio_data_store[dio_key(channel)].raw_value = (True if value == 1 else False)


def dio_write_resp(message):
    channel = message[1]
    value = message[2]
    valid = message[3]
    logger.debug('dio parse write response: valid={}, channel={}, value={}'.format(valid, channel, value))
    if valid:
        dio_data_store[dio_key(channel)].raw_value = (True if value == 1 else False)


# public


def dio_init(dio_item_id, setting):
    logger.debug('dio init {}'.format(dio_item_id))
    channel = external_dio_mapping[dio_item_id]['channel']
    dio_data_store[dio_key(channel)] = DioData(channel, setting)

    # parse setting
    if type(setting) is bool:
        setting = 1 if setting else 2
    elif type(setting) is int:
        setting = 1 if setting == 1 else 2
    elif type(setting) is str:
        setting = 1 if setting in ['Input', 'input', 'I', 'i'] else 2
    else:
        logger.error('Error: Cannot init DIO, cannot parse setting value')

    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['INIT'] & 0xF)
    message[1] = channel
    message[2] = setting
    message[7] = 0x17
    return message


def dio_read(dio_item_id):
    logger.debug('dio read {}'.format(dio_item_id))
    channel = external_dio_mapping[dio_item_id]['channel']

    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['READ'] & 0xF)
    message[1] = channel
    message[7] = 0x17
    return message


def dio_write(dio_item_id, value):
    logger.debug('dio write {}'.format(dio_item_id))
    channel = external_dio_mapping[dio_item_id]['channel']

    # parse value
    if type(value) is bool:
        value = 1 if value else 0
    elif type(value) is int:
        value = 1 if value == 1 else 0
    elif type(value) is str:
        value = 1 if value in ['High', 'True'] else 0
    else:
        logger.error('DIO write value error')
        return bytearray(8)

    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['WRITE'] & 0xF)
    message[1] = channel
    message[2] = value
    message[7] = 0x17
    return message


def dio_get_init(dio_item_id):
    # logger.debug('dio get init {}'.format(dio_item_id))
    channel = external_dio_mapping[dio_item_id]['channel']
    return dio_data_store[dio_key(channel)].init


def dio_get_data(dio_item_id):
    # logger.debug('dio get data {}'.format(dio_item_id))
    channel = external_dio_mapping[dio_item_id]['channel']
    return dio_data_store[dio_key(channel)].raw_value


def dio_parse_resp(command, message):
    logger.debug('dio parse response')
    if command == COMMANDS['INIT']:
        dio_init_resp(message)
    elif command == COMMANDS['READ']:
        dio_read_resp(message)
    elif command == COMMANDS['WRITE']:
        dio_write_resp(message)
    else:
        logger.error('Error: cannot parse response')


