import logging
logger = logging.getLogger('PATH')

MODULE = 6

COMMANDS = {
    'ENABLE': 0,
    'ISET': 1,
    'ISENSE': 2,
    'VSET': 3,
    'VSENSE': 4,
}

# private


def supply_enable_resp(message):
    valid = message[2]
    logger.debug('supply parse supply_enable_resp response: valid={}, '.format(valid))


def supply_set_current_resp(message):
    valid = message[3]
    logger.debug('supply parse supply_set_current_resp response: valid={}, '.format(valid))


def supply_get_current_resp(message):
    valid = message[3]
    value = int(((message[1] & 0xF) << 8) | message[2])
    logger.debug('supply parse supply_get_current_resp response: valid={}, value={}'.format(valid, value))


def supply_set_voltage_resp(message):
    valid = message[3]
    logger.debug('supply parse supply_set_voltage_resp response: valid={}, '.format(valid))


def supply_get_voltage_resp(message):
    valid = message[3]
    value = int(((message[1] & 0xF) << 8) | message[2])
    logger.debug('supply parse supply_get_voltage response: valid={}, value={}'.format(valid, value))

# public


def supply_enable():
    logger.debug('supply enable')
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ENABLE'] & 0xF)
    message[1] = 1
    message[7] = 0x17
    return message


def supply_set_current(value):
    logger.debug('supply set current {}'.format(value))
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ISET'] & 0xF)
    message[1] = (value >> 8 & 0b11)
    message[2] = (value & 0xFF)
    message[7] = 0x17
    return message


def supply_get_current():
    logger.debug('supply get current {}')
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['ISENSE'] & 0xF)
    message[7] = 0x17
    return message


def supply_set_voltage(value):
    logger.debug('supply set voltage {}'.format(value))
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['VSET'] & 0xF)
    message[1] = (value >> 8 & 0b11)
    message[2] = (value & 0xFF)
    message[7] = 0x17
    return message


def supply_get_voltage():
    logger.debug('supply get voltage {}')
    message = bytearray(8)
    message[0] = ((MODULE & 0xF) << 4) | (COMMANDS['VSENSE'] & 0xF)
    message[7] = 0x17
    return message


def supply_parse_resp(command, message):
    logger.debug('supply parse response')
    if command == COMMANDS['ENABLE']:
        supply_enable_resp(message)
    elif command == COMMANDS['ISET']:
        supply_set_current_resp(message)
    elif command == COMMANDS['ISENSE']:
        supply_get_current_resp(message)
    elif command == COMMANDS['VSET']:
        supply_set_voltage_resp(message)
    elif command == COMMANDS['VSENSE']:
        supply_get_voltage_resp(message)


