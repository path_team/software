import logging, pathlib, datetime

custom_handler_function = None


class CustomHandler(logging.StreamHandler):

    def emit(self, record):
        if record.levelname == 'ERROR':
            msg = self.format(record)
            if custom_handler_function:
                custom_handler_function(msg)


def set_custom_handler_function(f):
    global custom_handler_function
    custom_handler_function = f


def setup_custom_logger(name):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    custom = CustomHandler()
    custom.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    path = pathlib.Path(__file__).parent.absolute()

    date = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
    logging.basicConfig(filename=str(path)+'/'+date+'.log', level=logging.DEBUG)
    logger.addHandler(handler)
    logger.addHandler(custom)
    return logger