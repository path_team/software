import queue, time
from ..log import logger as my_logger
from ..api.uart_thread import UartThread
from ..api.adc_api import adc_init, adc_read, adc_get_init, adc_get_data


if __name__ == "__main__":
    logger = my_logger.setup_custom_logger('PATH')

    q = queue.Queue()
    api = UartThread(q) # creates a new thread that will handle the sending and receiving of uart messages
    api.start() # start that thread

    q.put(adc_init(1))  # initializes adc external output 1

    while not adc_get_init(1):
        continue

    while 1:
        q.put(adc_read(1))  # ask to read adc external output 1
        data = adc_get_data(1)
        print(data)
        time.sleep(1)

