import logging
from api.spi_api import spi_init, spi_get_init, spi_write, spi_get_response_data
from PySide2.QtWidgets import QCheckBox, QSpinBox, QPushButton, QTextEdit, QComboBox, QGridLayout, QFileDialog
from PySide2.QtCore import Qt, QTimer, QStringListModel
logger = logging.getLogger('PATH')


class SpiTransaction:

    def __init__(self, name, length, data):

        if type(data) != bytes:
            logger.error('ERROR: SpiTransaction wants data as bytes type')

        self.name = name
        self.length = length
        self.data = data

    def update(self, length=None, data=None):
        if length:
            self.length = length
        if data:
            self.data = data


class SpiController:

    def __init__(self, ui, _, api):
        logger.debug('init')

        self.ui = ui
        self.api = api

        self.transaction_store = {}
        self.transaction_model = QStringListModel(['<Transactions>'])
        self.spi_filepath = ""

        self.enable_button = self.ui.findChild(QCheckBox, 'enable')
        self.transaction_selection = self.ui.findChild(QComboBox, 'transactions')
        self.transaction_selection.setModel(self.transaction_model)
        self.name = self.ui.findChild(QTextEdit, 'name')
        self.length = self.ui.findChild(QSpinBox, 'length')
        self.data_grid = self.ui.findChild(QGridLayout, 'data_grid')
        self.data_grid_per_row = 8
        self.add_button = self.ui.findChild(QPushButton, 'add')
        self.remove_button = self.ui.findChild(QPushButton, 'remove')
        self.load_button = self.ui.findChild(QPushButton, 'load')
        self.save_button = self.ui.findChild(QPushButton, 'save')
        self.start_button = self.ui.findChild(QPushButton, 'start')
        self.results_text = self.ui.findChild(QTextEdit, 'results')

        self.enable_timer = QTimer()
        self.enable_timer.timeout.connect(self.update_enable)
        self.transaction_timer = QTimer()
        self.transaction_timer.timeout.connect(self.update_results)

        self.enable_button.clicked.connect(self.set_enable)
        self.transaction_selection.currentTextChanged.connect(self.set_selected_transaction)
        self.name.textChanged.connect(self.set_name)
        self.length.valueChanged.connect(self.set_length)
        self.add_button.clicked.connect(self.add_transaction)
        self.remove_button.clicked.connect(self.remove_transaction)
        self.load_button.clicked.connect(self.load_transactions_from_file)
        self.save_button.clicked.connect(self.save_transactions_to_file)
        self.start_button.clicked.connect(self.start_transaction)

        self.length.setValue(8)
        # self.enable_qt_elements(Qt.Unchecked)

    def enable_qt_elements(self, state=Qt.Checked):
        enable = state == Qt.Checked
        qt_elements = [self.enable_button, self.transaction_selection, self.name, self.length, self.data_grid,
                       self.add_button, self.remove_button, self.load_button, self.save_button, self.start_button,
                       self.results_text]
        for qt_e in qt_elements:
            qt_e.setEnabled(enable)

    def set_enable(self, state=Qt.Checked):
        if state == Qt.Checked:
            logger.debug('enable')
            self.api.put(spi_init())
            self.enable_timer.start(100)
        else:
            logger.debug('disable')
            self.enable_timer.stop()

    def update_enable(self):
        if spi_get_init():
            self.enable_timer.stop()
            self.enable_qt_elements()

    def set_selected_transaction(self, transaction_name):
        logger.debug('set_selected_transaction')

        if transaction_name in self.transaction_store:
            self.name.setPlainText(transaction_name)
            self.length.setValue(self.transaction_store[transaction_name].length)
            self._set_data(self.transaction_store[transaction_name].data)
        else:
            self.name.setPlainText("")
            self.length.setValue(8)
            self._set_data([])

    def set_name(self):
        logger.debug('set_name')

        transaction_name = self.name.toPlainText()

        if transaction_name in self.transaction_store:
            self.add_button.setText("Modify")
            self.remove_button.setEnabled(True)
        else:
            self.add_button.setText("Add")
            self.remove_button.setEnabled(True)

    def set_length(self, length):
        logger.debug('set_length')

        c = self.data_grid.count()

        if length == c:
            return

        while c != length:
            if c < length:
                byte_widget = QSpinBox()
                byte_widget.setDisplayIntegerBase(16)
                byte_widget.setPrefix('0x')
                byte_widget.setMaximum(255)
                self.data_grid.addWidget(byte_widget, int(c/self.data_grid_per_row), int(c%self.data_grid_per_row))
            else:
                widget = self.data_grid.takeAt(c-1).widget()
                self.data_grid.removeWidget(widget)
                widget.deleteLater()
            c = self.data_grid.count()

    def _update_transaction_model(self):
        self.transaction_model.layoutAboutToBeChanged.emit()
        transaction_names_list = [self.transaction_store[t].name for t in self.transaction_store]
        self.transaction_model.setStringList(['<Transactions>'] + transaction_names_list)
        self.transaction_model.layoutChanged.emit()
        self.transaction_selection.setCurrentIndex(0)

    def add_transaction(self):
        logger.debug('add_transaction')

        transaction_name = self.name.toPlainText()
        length = self.length.value()
        data = self._get_data()

        if transaction_name in self.transaction_store:
            self.transaction_store[transaction_name].update(length=length, data=data)
        else:
            self.transaction_store[transaction_name] = SpiTransaction(transaction_name, length, data)
            self._update_transaction_model()

    def remove_transaction(self):
        logger.debug('remove_transaction')

        transaction_name = self.name.toPlainText()

        if transaction_name in self.transaction_store:
            self.transaction_store.pop(transaction_name)
            self._update_transaction_model()

    def load_transactions_from_file(self):
        logger.debug('load_transactions_from_file')

        dialog = QFileDialog()
        dialog.setOption(QFileDialog.DontUseNativeDialog)
        (file_path, filter) = dialog.getOpenFileName(self.ui, "Open SPI File", self.spi_filepath, "SPI (*.spi)")
        self.spi_filepath = file_path
        spi_file = open(file_path, 'r')
        lines = spi_file.readlines()
        spi_file.close()
        for line in lines[1:]:
            transaction_data = line.split()
            transaction = SpiTransaction(transaction_data[0], int(transaction_data[1]), bytes.fromhex(transaction_data[2]))
            self.transaction_store[transaction_data[0]] = transaction
        self._update_transaction_model()

    def save_transactions_to_file(self):
        logger.debug('save_transactions_to_file')

        dialog = QFileDialog()
        dialog.setOption(QFileDialog.DontUseNativeDialog)
        (file_path, filter) = dialog.getSaveFileName(self.ui, "Save SPI File", self.spi_filepath, "SPI (*.spi)")
        self.spi_filepath = file_path
        spi_file = open(file_path, 'w')
        spi_file.write("{} {} {}\n".format("Name", "Length", "Data"))
        for transaction_name in self.transaction_store:
            transaction = self.transaction_store[transaction_name]
            spi_file.write("{} {} {}\n".format(transaction.name, transaction.length, transaction.data.hex()))
        spi_file.close()

    def start_transaction(self):
        logger.debug('start_transaction')

        for message in spi_write(self._get_data(), self.length.value()):
            self.api.put(message)
        self.transaction_timer.start(100)

    def update_results(self):
        logger.debug('update_results')

        if spi_get_response_data():
            self.results_text.setPlainText(bytes(spi_get_response_data()).hex())
            self.transaction_timer.stop()

    def _set_data(self, data):
        logger.debug('_set_data')

        if len(data) == self.data_grid.count():
            for i, value in enumerate(data):
                self.data_grid.itemAt(i).widget().setValue(value)

    def _get_data(self):
        logger.debug('_get_data')
        data = bytes()
        for i in reversed(range(self.data_grid.count())):
            data += bytes([self.data_grid.itemAt(i).widget().value()])
        return data