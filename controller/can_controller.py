import logging, cantools
from api.can_api import can_init, can_get_init, can_get_rx_messages, can_write_message_once, can_write_message_frequency
from PySide2.QtWidgets import QCheckBox, QSpinBox, QDoubleSpinBox, QPushButton, QTreeView, QComboBox, QFileDialog
from PySide2.QtCore import Qt, QTimer, QStringListModel
from model.treeModel import TreeModel
logger = logging.getLogger('PATH')


class CanController:

    def __init__(self, ui, _, api):
        logger.debug('init')

        self.ui = ui
        self.api = api

        # globals
        self.dbc = None
        self.dbc_filepath = ""
        self.tx_messages = {}
        self.rx_messages = {}
        self.rx_model_columns = ['ID', 'DLC', 'Data', 'Count']
        self.rx_model = TreeModel(self.rx_model_columns, '')
        self.tx_messages_model = QStringListModel(['<Messages>'])
        self.tx_signals_model = QStringListModel(['<Signals>'])

        # ui elements
        # top
        self.enable_checkbox = self.ui.findChild(QCheckBox, 'enable')
        self.baud_rate_selection = self.ui.findChild(QComboBox, 'baudrate')
        self.dbc_button = self.ui.findChild(QPushButton, 'dbc')

        # tx
        self.tx_message_selection = self.ui.findChild(QComboBox, 'messages')
        self.tx_message_selection.setModel(self.tx_messages_model)
        self.tx_message_id = self.ui.findChild(QSpinBox, 'message_id')
        for i in range(8):
            setattr(self, 'tx_message_raw_{}_num'.format(i), self.ui.findChild(QSpinBox, 'message_raw_{}'.format(i)))
        self.tx_signal_selection = self.ui.findChild(QComboBox, 'signals')
        self.tx_signal_selection.setModel(self.tx_signals_model)
        self.tx_signal_value = self.ui.findChild(QDoubleSpinBox, 'signal_value')
        self.tx_recurring_checkbox = self.ui.findChild(QCheckBox, 'recurring')
        self.tx_frequency = self.ui.findChild(QSpinBox, 'frequency')
        self.tx_send_button = self.ui.findChild(QPushButton, 'send')

        # rx
        self.rx_tree_view = self.ui.findChild(QTreeView, 'rx')

        # timers
        self.enable_timer = QTimer()
        self.enable_timer.timeout.connect(self.update_enable)
        self.rx_timer = QTimer()
        self.rx_timer.timeout.connect(self.update_rx)

        # connections
        self.dbc_button.clicked.connect(self.set_dbc)
        self.enable_checkbox.stateChanged.connect(self.set_enable)
        self.baud_rate_selection.currentTextChanged.connect(self.set_enable)
        self.tx_message_selection.currentTextChanged.connect(self.set_selected_tx_message)
        self.tx_signal_selection.currentTextChanged.connect(self.set_selected_tx_signal)
        self.tx_signal_value.valueChanged.connect(self.set_tx_signal_value)
        self.tx_recurring_checkbox.stateChanged.connect(self.set_tx_recurring)
        self.tx_send_button.clicked.connect(self.send_tx_message)
        self.rx_tree_view.setModel(self.rx_model)

        # initial states
        # self.enable_qt_elements(Qt.Unchecked)
        # self.tx_frequency.setEnabled(False)

    def close(self):
        logger.debug('close')

    def enable_qt_elements(self, state=Qt.Checked):
        enable = state == Qt.Checked
        qt_elements = [self.dbc_button,
                       self.tx_message_id,
                       self.tx_message_selection,
                       self.tx_signal_selection,
                       # self.tx_signal_value,
                       self.tx_recurring_checkbox,
                       # self.tx_frequency,
                       self.tx_send_button,
                       self.rx_tree_view]
        qt_elements += [getattr(self, 'tx_message_raw_{}_num'.format(i)) for i in range(8)]
        for qt_e in qt_elements:
            qt_e.setEnabled(enable)

    def set_enable(self, state=Qt.Checked):
        if state == Qt.Checked:
            logger.debug('enable')
            baud_rate_index = self.baud_rate_selection.currentIndex()
            self.api.put(can_init(baud_rate_index))
            self.enable_timer.start(100)
        else:
            logger.debug('disable')
            self.enable_timer.stop()
            self.rx_timer.stop()

    def update_enable(self):
        if can_get_init():
            self.enable_timer.stop()
            self.enable_qt_elements()
            self.rx_timer.start(10)

    def update_rx(self):
        messages = can_get_rx_messages()
        for message_id in messages:
            if message_id in self.rx_messages:
                self.rx_model.updateRowData(self.rx_messages[message_id], [message_id, messages[message_id]['length'],
                                                                           messages[message_id]['data'].hex(),
                                                                           messages[message_id]['count']])
            else:
                last_row = self.rx_model.rowCount()
                self.rx_model.insertRowDataAt(last_row,
                                              [message_id, messages[message_id]['length'],
                                               messages[message_id]['data'].hex(),
                                               messages[message_id]['count']])

                self.rx_messages[message_id] = last_row

    def set_dbc(self):
        dialog = QFileDialog()
        dialog.setOption(QFileDialog.DontUseNativeDialog)
        (file_path, filter) = dialog.getOpenFileName(self.ui, "Open DBC", self.dbc_filepath, "DBC (*.dbc)")
        self.dbc_filepath = file_path
        self.dbc = cantools.db.load_file(file_path)
        if self.dbc:
            message_names_list = [message.name for message in self.dbc.messages]
            self.tx_messages_model.layoutAboutToBeChanged.emit()
            self.tx_messages_model.setStringList(['<Messages>'] + message_names_list)
            self.tx_messages_model.layoutChanged.emit()
            self.tx_message_selection.setCurrentIndex(0)

    def set_selected_tx_message(self, message_name):
        logger.debug('set_selected_tx_message {}'.format(message_name))

        if not self.dbc:
            logger.error('ERROR: invalid dbc')
            return

        if not hasattr(self.dbc, 'messages'):
            logger.error('ERROR: invalid dbc messages')
            return

        try:
            message = self.dbc.get_message_by_name(message_name)
            signal_names_list = [signal.name for signal in message.signals]
            self.tx_signals_model.layoutAboutToBeChanged.emit()
            self.tx_signals_model.setStringList(['<Signals>'] + signal_names_list)
            self.tx_signals_model.layoutChanged.emit()
            self.tx_signal_selection.setCurrentIndex(0)
            self.tx_signal_value.setEnabled(False)
        except KeyError:
            logger.error('ERROR: Could not find message {}'.format(message_name))

    def set_selected_tx_signal(self, signal_name):
        logger.debug('set_selected_tx_signal {}'.format(signal_name))
        if not self.dbc:
            logger.error('ERROR: invalid dbc')
            return

        if not hasattr(self.dbc, 'messages'):
            logger.error('ERROR: invalid dbc messages')
            return

        message_name = self.tx_message_selection.currentText()

        try:
            message = self.dbc.get_message_by_name(message_name)
            if signal_name in message.signal_tree:
                self.tx_signal_value.setEnabled(True)
            else:
                self.tx_signal_value.setEnabled(False)
        except KeyError:
            logger.error('ERROR: Could not find message {}'.format(message_name))

    def set_tx_signal_value(self, signal_value):
        logger.debug('set_tx_signal_value {}'.format(signal_value))
        if not self.dbc:
            logger.error('ERROR: invalid dbc')
            return

        if not hasattr(self.dbc, 'messages'):
            logger.error('ERROR: invalid dbc messages')
            return

        message_name = self.tx_message_selection.currentText()
        signal_name = self.tx_signal_selection.currentText()

        message = self.dbc.get_message_by_name(message_name)

        if message:
            if signal_name in message.signal_tree:
                if message_name not in self.tx_messages:
                    self.tx_messages[message_name] = {signal.name: 0 for signal in message.signals}
                self.tx_messages[message_name][signal_name] = signal_value
                data = message.encode(self.tx_messages[message_name])
                while len(data) != 8:
                    data += b'\x00'
                self.set_message_bytes(data)

    def set_message_bytes(self, message_bytes):
        logger.debug('set_bytes {}'.format(message_bytes))

        if not message_bytes or type(message_bytes) != bytes or  len(message_bytes) != 8:
            logger.error('ERROR: invalid input')
            return

        for i in range(8):
            getattr(self, 'tx_message_raw_{}_num'.format(i)).setValue(message_bytes[i])

    def set_tx_recurring(self, state):
        logger.debug('set_tx_recurring {}'.format(str(state)))

        if state == Qt.Checked:
            self.tx_frequency.setEnabled(True)
        else:
            self.tx_frequency.setEnabled(False)

    def send_tx_message(self):
        logger.debug('send tx message')

        message_id = self.tx_message_id.value()
        message_data = bytes()
        for i in reversed(range(8)):
            message_data += bytes([getattr(self, 'tx_message_raw_{}_num'.format(i)).value()])

        if self.tx_recurring_checkbox.checkState() == Qt.Checked:
            frequency = self.tx_frequency.value()
            for api_msg in can_write_message_frequency(message_id, message_data, frequency):
                self.api.put(api_msg)
        else:
            for api_msg in can_write_message_once(message_id, message_data):
                self.api.put(api_msg)
