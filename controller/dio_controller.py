import logging
from .dio_item_controller import DioItem
from PySide2.QtWidgets import QGridLayout
logger = logging.getLogger('PATH')


class DioController:

    def __init__(self, ui, dios_uis, api):
        logger.debug('init')
        self.ui = ui
        self.api = api

        self.grid = self.ui.findChild(QGridLayout, 'grid')
        self.grid.setHorizontalSpacing(0)
        self.grid.setVerticalSpacing(0)
        self.dio_items = {}
        for i, dio_ui in enumerate(dios_uis):
            self.dio_items[i] = DioItem(ui=dio_ui, id=i+1, api=api)     # +1 so they're the same as in the schematic

        self.add_items_to_grid(max_column=5)

    def close(self):
        logger.debug('close')

    def add_items_to_grid(self, max_column):
        row = 0
        column = 0
        i = 0
        for item in self.dio_items:
            self.grid.addWidget(self.dio_items[item].ui, row, column)
            i += 1
            column += 1
            if column >= max_column:
                row += 1
                column = 0

    def set_value(self, dio_item_name, value):
        logger.debug('Set {} to {}'.format(dio_item_name, value))
        if dio_item_name in self.dio_items:
            self.dio_items[dio_item_name].set_value(value)
        else:
            logger.error('ERROR: DIO value could not be set, no dio with name: {}'.format(dio_item_name))


