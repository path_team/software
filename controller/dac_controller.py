import logging
from .dac_item_controller import DacItem
from PySide2.QtWidgets import QGridLayout
logger = logging.getLogger('PATH')


class DacController:

    def __init__(self, ui, dacs_uis, api):
        logger.debug('init')
        self.ui = ui
        self.api = api

        self.grid = self.ui.findChild(QGridLayout, 'grid')
        self.grid.setHorizontalSpacing(0)
        self.grid.setVerticalSpacing(0)
        self.dac_items = {}

        dac_ids = ['EXT-1', 'EXT-2', 'EXT-3', 'EXT-4', 'EXT-5', 'EXT-6', 'EXT-7', 'EXT-8', 'EXT-9', 'EXT-10', 'INT-SUPPLY-ISET', 'INT-SUPPLY-VSET', 'INT-LOAD-ISET']

        for i, dac_ui in enumerate(dacs_uis):
            self.dac_items[i] = DacItem(ui=dac_ui, id=dac_ids[i], api=api)     # +1 so they're the same as in the schematic

        self.add_items_to_grid(max_column=5)

    def close(self):
        logger.debug('close')

    def add_items_to_grid(self, max_column):
        row = 0
        column = 0
        i = 0
        for item in self.dac_items:
            self.grid.addWidget(self.dac_items[item].ui, row, column)
            i += 1
            column += 1
            if column >= max_column:
                row += 1
                column = 0

