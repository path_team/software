import logging
from .adc_item_controller import AdcItem
from PySide2.QtWidgets import QGridLayout
logger = logging.getLogger('PATH')


class AdcController:

    def __init__(self, ui, adcs_uis, api):
        logger.debug('init')
        self.ui = ui
        self.api = api

        self.grid = self.ui.findChild(QGridLayout, 'grid')
        self.grid.setHorizontalSpacing(0)
        self.grid.setVerticalSpacing(0)
        self.adc_items = {}
        for i, adc_ui in enumerate(adcs_uis):
            self.adc_items[i] = AdcItem(ui=adc_ui, id=i+1, api=api)     # +1 so they're the same as in the schematic

        self.add_items_to_grid(max_column=5)

    def close(self):
        logger.debug('close')

    def add_items_to_grid(self, max_column):
        row = 0
        column = 0
        i = 0
        for item in self.adc_items:
            self.grid.addWidget(self.adc_items[item].ui, row, column)
            i += 1
            column += 1
            if column >= max_column:
                row += 1
                column = 0

    def set_value(self, adc_item_name, value):
        logger.debug('Set {} to {}'.format(adc_item_name, value))
        if adc_item_name in self.adc_items:
            self.adc_items[adc_item_name].set_value(value)
        else:
            logger.error('ERROR: ADC value could not be set, no adc with name: {}'.format(adc_item_name))


