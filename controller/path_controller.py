import logging, pathlib
from log import logger as my_logger
from .adc_controller import AdcController
from .dac_controller import DacController
from .dio_controller import DioController
from .can_controller import CanController
from .spi_controller import SpiController
from PySide2.QtWidgets import QApplication, QAction, QDockWidget, QStatusBar
from PySide2.QtGui import QIcon
from PySide2.QtCore import QFile, QTimer, Qt
from PySide2.QtUiTools import QUiLoader

logger = logging.getLogger('PATH')
views = pathlib.Path(__file__).parent.absolute() / '..' / 'view'
resources = pathlib.Path(__file__).parent.absolute() / '..' / 'resources'


def load_ui(file_path, parent=None):
    logger.debug('opening: ' + str(file_path))
    try:
        ui_file = QFile(str(file_path))
        ui_file.open(QFile.ReadOnly)
        _ui = QUiLoader().load(ui_file, parent)
        ui_file.close()
        return _ui
    except RuntimeError as e:
        logger.error(e)
    return None


class PathController:

    def __init__(self, _api):
        super().__init__()
        logger.debug('init')

        self.app = QApplication([])
        self.api = _api

        self.ui = load_ui(str(views / 'main.ui'))
        self.ui.setWindowIcon(QIcon(str(resources / 'icon.png')))

        self.open_controllers = []

        self.status_bar = self.ui.findChild(QStatusBar, 'statusbar')
        my_logger.set_custom_handler_function(lambda msg: self.status_bar.showMessage(msg))

        self.adc_action = self.ui.findChild(QAction, 'menu_window_adc')
        self.install_controller(self.adc_action, AdcController, views / 'adc_view.ui', self.open_dock, self.close_dock, [views/'adc_item.ui' for i in range(10)])

        self.dac_action = self.ui.findChild(QAction, 'menu_window_dac')
        self.install_controller(self.dac_action, DacController, views / 'dac_view.ui', self.open_dock, self.close_dock, [views / 'dac_item.ui' for i in range(13)])

        self.dio_action = self.ui.findChild(QAction, 'menu_window_dio')
        self.install_controller(self.dio_action, DioController, views / 'dio_view.ui', self.open_dock, self.close_dock, [views/'dio_item.ui' for i in range(10)])

        self.can_action = self.ui.findChild(QAction, 'menu_window_can')
        self.install_controller(self.can_action, CanController, views / 'can_view.ui', self.open_dock, self.close_dock)

        self.spi_action = self.ui.findChild(QAction, 'menu_window_spi')
        self.install_controller(self.spi_action, SpiController, views / 'spi_view.ui', self.open_dock, self.close_dock)

        self.ui.showMaximized()

    def close(self):
        logger.debug('close')
        for controller in self.open_controllers:
            controller.close()

    def install_controller(self, action, controller_class, controller_ui_file, open_f, close_f, ui_file_args=[]):

        def open_controller():

            controller_ui = load_ui(controller_ui_file, self.ui)

            ui_args = []
            for ui_file in ui_file_args:
                ui_args.append(load_ui(ui_file, self.ui))

            controller = controller_class(controller_ui, ui_args, self.api)
            open_f(controller)

            if close_f:
                def check_closed():  # kind of a hack but much simpler than the other ways of detecting a close
                    if not controller_ui.isVisible():
                        close_f(controller)
                        action.setEnabled(True)
                        close_timer.stop()
                        self.open_controllers.remove(controller)

                action.setEnabled(False)
                close_timer = QTimer()
                close_timer.timeout.connect(check_closed)
                close_timer.start(1)

            self.open_controllers.append(controller)

        action.triggered.connect(open_controller)

    def open_window(self, controller):
        controller.ui.show()

    def open_dock(self, controller):
        controller.dockable = self.dockify(controller.ui)
        self.ui.addDockWidget(Qt.TopDockWidgetArea, controller.dockable)
        if self.open_controllers:
            self.ui.tabifyDockWidget(self.open_controllers[0].dockable, controller.dockable)
        self.ui.resizeDocks([controller.dockable], [self.ui.height()], Qt.Vertical)
        controller.ui.show()

    def close_window(self, controller):
        controller.close()
        del controller

    def close_dock(self, controller):
        self.ui.removeDockWidget(controller.dockable)
        controller.close()
        del controller

    def dockify(self, widget):
        dockable = QDockWidget()
        dockable.setWidget(widget)
        dockable.setStyleSheet("QDockWidget > QWidget {border: 1px solid grey}")
        dockable.setWindowTitle(widget.windowTitle())
        return dockable

