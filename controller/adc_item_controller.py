import logging
from api.adc_api import adc_init, adc_read, adc_get_init, adc_get_data
from PySide2.QtWidgets import QTextEdit, QSpinBox, QDoubleSpinBox, QCheckBox, QPushButton
from PySide2.QtCore import Qt, QTimer
logger = logging.getLogger('PATH')


class AdcItem:

    def __init__(self, ui, id, api):
        self.ui = ui
        self.id = id
        self.api = api

        # get all fields from ui file
        self.enable_checkbox = self.ui.findChild(QCheckBox, 'enable')
        self.name = self.ui.findChild(QTextEdit, 'name')
        self.units = self.ui.findChild(QTextEdit, 'units')
        self.value = self.ui.findChild(QDoubleSpinBox, 'value')
        self.scaled_checkbox = self.ui.findChild(QCheckBox, 'scaled')
        self.scalar = self.ui.findChild(QDoubleSpinBox, 'scalar')
        self.offset = self.ui.findChild(QDoubleSpinBox, 'offset')
        self.autoread_checkbox = self.ui.findChild(QCheckBox, 'autoread')
        self.read_frequency = self.ui.findChild(QSpinBox, 'frequency')
        self.read_button = self.ui.findChild(QPushButton, 'read')

        # setup timers for polling api for result
        self.init_timer = QTimer()
        self.init_timer.timeout.connect(self.update_init)
        self.read_value_timer = QTimer()
        self.read_value_timer.timeout.connect(self.update_value)

        # setup ui elements
        self.name.setPlainText('ADC '+str(id))
        self.enable_checkbox.stateChanged.connect(self.enable)
        self.units.textChanged.connect(lambda: self.value.setSuffix(' ' + self.units.toPlainText()))
        self.autoread_checkbox.stateChanged.connect(self.update_autoread)
        self.scaled_checkbox.stateChanged.connect(self.update_scaled)
        self.read_button.clicked.connect(self.update_read)
        self.update_autoread(Qt.Unchecked)
        self.update_scaled(Qt.Unchecked)

        # disable all ui elements until init is complete (see update_init)
        self.name.setEnabled(False)
        self.units.setEnabled(False)
        self.value.setEnabled(False)
        self.scaled_checkbox.setEnabled(False)
        self.scalar.setEnabled(False)
        self.offset.setEnabled(False)
        self.autoread_checkbox.setEnabled(False)
        self.read_frequency.setEnabled(False)
        self.read_button.setEnabled(False)

    def enable(self, state):
        if state == Qt.Checked:
            logger.debug('{} init'.format(self.name.toPlainText()))
            self.api.put(adc_init(self.id))
            self.init_timer.start(100)
        else:
            logger.debug('{} deinit'.format(self.name.toPlainText()))
            # TODO: deinit

    def update_init(self):

        if adc_get_init(self.id):
            self.init_timer.stop()
            self.name.setEnabled(True)
            self.units.setEnabled(True)
            self.value.setEnabled(True)
            self.scaled_checkbox.setEnabled(True)
            self.scalar.setEnabled(True)
            self.offset.setEnabled(True)
            self.autoread_checkbox.setEnabled(True)
            self.read_frequency.setEnabled(True)
            self.read_button.setEnabled(True)

    def update_autoread(self, state):
        if state == Qt.Checked:
            # logger.debug('{} autoread checked'.format(self.name.toPlainText()))
            self.read_frequency.setVisible(True)
        else:
            # logger.debug('{} autoread unchecked'.format(self.name.toPlainText()))
            self.read_frequency.setVisible(False)
            self.read_value_timer.stop()

    def update_scaled(self, state):
        if state == Qt.Checked:
            # logger.debug('{} scaled checked'.format(self.name.toPlainText()))
            self.scalar.setVisible(True)
            self.offset.setVisible(True)
        else:
            # logger.debug('{} scaled unchecked'.format(self.name.toPlainText()))
            self.scalar.setVisible(False)
            self.offset.setVisible(False)

    def update_read(self):
        if self.autoread_checkbox.checkState() == Qt.Checked:
            frequency = self.read_frequency.value()
            if self.read_button.text() == 'Stop':
                logger.debug('{} stop reading'.format(self.name.toPlainText()))
                self.read_button.setText('Read')
                self.read_value_timer.stop()
            else:
                logger.debug('{} read at frequency {}'.format(self.name.toPlainText(), frequency))
                self.read_button.setText('Stop')
                self.api.put(adc_read(self.id))
                self.read_value_timer.start(1000 / frequency)
        else:
            logger.debug('{} read once'.format(self.name.toPlainText()))
            self.api.put(adc_read(self.id))
            self.read_value_timer.start(100)

    def update_value(self):
        value = adc_get_data(self.id)
        logger.debug('{} raw value {}'.format(self.name.toPlainText(), value))

        if self.autoread_checkbox.checkState() != Qt.Checked:
            self.read_value_timer.stop()    # stops update timer if not autoread
        else:
            self.api.put(adc_read(self.id))

        if self.scaled_checkbox.checkState() == Qt.Checked:
            value *= self.scalar.value()
            value += self.offset.value()

        self.value.setValue(value)
