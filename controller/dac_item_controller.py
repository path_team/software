import logging
from api.dac_api import dac_set_raw, dac_get_set_status
from PySide2.QtWidgets import QTextEdit, QSpinBox, QDoubleSpinBox, QCheckBox, QPushButton, QComboBox
from PySide2.QtCore import Qt, QTimer
logger = logging.getLogger('PATH')


class DacItem:

    def __init__(self, ui, id, api):
        self.ui = ui
        self.id = id
        self.api = api

        # get all fields from ui file
        self.enable_checkbox = self.ui.findChild(QCheckBox, 'enable')
        self.name = self.ui.findChild(QTextEdit, 'name')
        self.units = self.ui.findChild(QComboBox, 'units')
        self.value = self.ui.findChild(QDoubleSpinBox, 'value')
        self.set_button = self.ui.findChild(QPushButton, 'set')

        # timers
        self.set_timer = QTimer()
        self.set_timer.timeout.connect(self.update_set)

        # setup ui elements
        self.name.setPlainText('DAC '+str(id))
        self.enable_checkbox.stateChanged.connect(self.enable)
        self.set_button.clicked.connect(self.set_raw_value)

        # disable all ui elements until init is complete (see update_init)
        self.enable_ui(False)

    def enable_ui(self, enable=True):
        self.name.setEnabled(enable)
        self.units.setEnabled(enable)
        self.value.setEnabled(enable)
        self.set_button.setEnabled(enable)

    def enable(self, state):
        if state == Qt.Checked:
            logger.debug('{} init'.format(self.name.toPlainText()))
            self.enable_ui()
        else:
            logger.debug('{} deinit'.format(self.name.toPlainText()))
            self.enable_ui(False)

    def set_raw_value(self):
        value = self.value.value()
        self.set_button.setEnabled(False)
        logger.debug('update_set {} {} {}'.format(self.name.toPlainText(), self.id, value))
        self.api.put(dac_set_raw(self.id, value))
        self.set_timer.start(100)

    def update_set(self):
        if dac_get_set_status(self.id):
            self.set_button.setEnabled(True)
            self.set_timer.stop()

