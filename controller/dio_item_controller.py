import logging
from api.dio_api import dio_init, dio_read, dio_write, dio_get_init, dio_get_data
from PySide2.QtWidgets import QTextEdit, QSpinBox, QDoubleSpinBox, QCheckBox, QPushButton, QRadioButton
from PySide2.QtCore import Qt, QTimer
logger = logging.getLogger('PATH')


class DioItem:

    def __init__(self, ui, id, api):
        self.ui = ui
        self.id = id
        self.api = api

        # get all fields from ui file
        self.enable_checkbox = self.ui.findChild(QCheckBox, 'enable')
        self.input_radio = self.ui.findChild(QRadioButton, 'input')
        self.output_radio = self.ui.findChild(QRadioButton, 'output')
        self.name = self.ui.findChild(QTextEdit, 'name')
        self.value_checkbox = self.ui.findChild(QCheckBox, 'value')
        self.autoread_checkbox = self.ui.findChild(QCheckBox, 'autoread')
        self.autoread_frequency = self.ui.findChild(QSpinBox, 'frequency')
        self.read_button = self.ui.findChild(QPushButton, 'read')

        # setup timers for polling api for result
        self.setting_timer = QTimer()
        self.setting_timer.timeout.connect(self.update_setting)
        self.read_value_timer = QTimer()
        self.read_value_timer.timeout.connect(self.update_value)

        # setup ui elements
        self.name.setPlainText('DIO '+str(id))
        self.enable_checkbox.stateChanged.connect(lambda checked: self.set_setting('Input') if checked == Qt.Checked else self.deinit())
        self.input_radio.clicked.connect(lambda: self.set_setting('Input'))
        self.output_radio.clicked.connect(lambda: self.set_setting('Output'))
        self.input_radio.clicked.connect(lambda checked: self.output_radio.setChecked(not checked))
        self.output_radio.clicked.connect(lambda checked: self.input_radio.setChecked(not checked))
        self.autoread_checkbox.stateChanged.connect(self.update_autoread)
        self.read_button.clicked.connect(self.update_read)
        self.value_checkbox.clicked.connect(self.set_write)

        # disable all ui elements until init is complete
        self._all(disable=True)

    def _all(self, disable=False, enable=False):
        enabled = not disable and enable
        self.update_autoread(Qt.Unchecked)
        self.input_radio.setEnabled(enabled)
        self.output_radio.setEnabled(enabled)
        self.name.setEnabled(enabled)
        self.value_checkbox.setEnabled(enabled)
        self.autoread_checkbox.setEnabled(enabled)
        self.autoread_frequency.setEnabled(enabled)
        self.read_button.setEnabled(enabled)

    def deinit(self):
        # TODO:
        pass

    def set_setting(self, setting):
        logger.debug('{} set to {}'.format(self.name.toPlainText(), setting))
        self._all(disable=True)
        self.api.put(dio_init(self.id, setting))
        self.setting_timer.start(100)

    def update_setting(self):
        if dio_get_init(self.id):
            logger.debug('{} setting updated'.format(self.name.toPlainText()))
            self.setting_timer.stop()
            self._all(enable=True)

    def set_write(self, value):
        logger.debug('{} write {}'.format(self.name.toPlainText(), value))
        self.api.put(dio_write(self.id, value))
        self.read_value_timer.start(100)

    def update_autoread(self, state):
        if state == Qt.Checked:
            # logger.debug('{} autoread checked'.format(self.name.toPlainText()))
            self.autoread_frequency.setVisible(True)
        else:
            # logger.debug('{} autoread unchecked'.format(self.name.toPlainText()))
            self.autoread_frequency.setVisible(False)
            self.read_value_timer.stop()

    def update_read(self):
        if self.autoread_checkbox.checkState() == Qt.Checked:
            frequency = self.autoread_frequency.value()
            if self.read_button.text() == 'Stop':
                logger.debug('{} stop reading'.format(self.name.toPlainText()))
                self.read_button.setText('Read')
                self.read_value_timer.stop()
            else:
                logger.debug('{} read at frequency {}'.format(self.name.toPlainText(), frequency))
                self.read_button.setText('Stop')
                self.api.put(dio_read(self.id))
                self.read_value_timer.start(1000 / frequency)
        else:
            logger.debug('{} read once'.format(self.name.toPlainText()))
            self.api.put(dio_read(self.id))
            self.read_value_timer.start(100)

    def update_value(self):
        value = dio_get_data(self.id)
        logger.debug('{} raw value {}'.format(self.name.toPlainText(), value))

        if self.autoread_checkbox.checkState() != Qt.Checked:
            self.read_value_timer.stop()    # stops update timer if not autoread
        else:
            self.api.put(dio_read(self.id))

        self.value_checkbox.setCheckState(Qt.Checked if value else Qt.Unchecked)
        self.value_checkbox.setText('HIGH' if value else 'LOW')
