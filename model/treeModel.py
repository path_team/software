import logging
from PySide2 import QtCore
logger = logging.getLogger('backend')

class TreeItem(object):
    def __init__(self, data, parent=None):
        self.parentItem = parent
        self.itemData = data
        self.childItems = []

    def appendChild(self, item):
        self.childItems.append(item)

    def child(self, row):
        return self.childItems[row]

    def childCount(self):
        return len(self.childItems)

    def childNumber(self):
        if self.parentItem != None:
            return self.parentItem.childItems.index(self)
        return 0

    def firstChild(self):
        return self.child(0)

    def lastChild(self):
        return self.child(self.childCount()-1)

    def columnCount(self):
        return len(self.itemData)

    def data(self, column):
        try:
            return self.itemData[column]
        except IndexError:
            return None

    def insertChildren(self, position, count, columns):
        if position < 0 or position > len(self.childItems):
            return False

        for row in range(count):
            data = [None for v in range(columns)]
            item = TreeItem(data, self)
            self.childItems.insert(position, item)

        return True

    def insertColumns(self, position, columns):
        if position < 0 or position > len(self.itemData):
            return False

        for column in range(columns):
            self.itemData.insert(position, None)

        for child in self.childItems:
            child.insertColumns(position, columns)

        return True

    def parent(self):
        return self.parentItem

    def removeChildren(self, position, count):
        if position < 0 or position + count > len(self.childItems):
            return False

        for row in range(count):
            self.childItems.pop(position)

        return True

    def removeColumns(self, position, columns):
        if position < 0 or position + columns > len(self.itemData):
            return False

        for column in range(columns):
            self.itemData.pop(position)

        for child in self.childItems:
            child.removeColumns(position, columns)

        return True

    def setData(self, column, value):
        if column < 0 or column >= len(self.itemData):
            return False
        if value is None:
            return False

        self.itemData[column] = value
        return True


class TreeModel(QtCore.QAbstractItemModel):
    def __init__(self, headers, data, parent=None):
        super(TreeModel, self).__init__(parent)
        rootData = [header for header in headers]
        self.rootItem = TreeItem(rootData)
        self.setupModelData(data.split("\n"), self.rootItem)

    def updateRowData(self, row, data, insert=False, parent=QtCore.QModelIndex()):
        self.layoutAboutToBeChanged.emit()
        try:
            if row < self.rowCount(parent):
                for i, d in enumerate(data):
                    self.setData(self.index(row, i, parent), str(d))
                self.layoutChanged.emit()
                return True
            elif insert:  # if not found, insert
                self.insertRowData(data, parent)
                self.layoutChanged.emit()
                return True
        except Exception as e:
            logger.error(str(e))
        return False

    def updateRowDataByColumnValue(self, column, value, data, insert=False, parent=QtCore.QModelIndex()):
        self.layoutAboutToBeChanged.emit()
        try:
            for row in range(self.rowCount(parent)):
                if self.data(self.index(row, column, parent)) == str(value):
                    for i, d in enumerate(data):
                        self.setData(self.index(row, i, parent), str(d))
                    self.layoutChanged.emit()
                    return True
            if insert:  # if not found, insert
                self.insertRowData(data, parent)
                self.layoutChanged.emit()
                return True
        except Exception as e:
            logger.error(str(e))
        return False

    def insertRowsDataAt(self, position, data, parent=QtCore.QModelIndex()):
        self.layoutAboutToBeChanged.emit()
        try:
            for row in data:
                self.insertRows(position, 1, parent)
                for i, d in enumerate(row):
                    self.setData(self.index(position, i, parent), str(d))
            self.layoutChanged.emit()
            return True
        except Exception as e:
            logger.error(str(e))
        return False

    def insertRowsData(self, data, parent=QtCore.QModelIndex()):
        self.insertRowsDataAt(0, data, parent)

    def insertRowDataAt(self, position, data, parent=QtCore.QModelIndex()):
        self.layoutAboutToBeChanged.emit()
        try:
            self.insertRows(position, 1, parent)
            for i, d in enumerate(data):
                self.setData(self.index(position, i, parent), str(d))
            self.layoutChanged.emit()
            return True
        except Exception as e:
            logger.error(str(e))
        return False

    def insertRowData(self, data, parent=QtCore.QModelIndex()):
        self.insertRowDataAt(0, data, parent)

    def getRowData(self, row, parent=QtCore.QModelIndex()):
        return [self.data(self.index(row, col, parent)) for col in range(self.columnCount(parent))]

    def insertColumnData(self, data, parent=QtCore.QModelIndex()):
        self.layoutAboutToBeChanged.emit()
        try:
            self.insertRows(0, len(data), parent)
            for i, d in enumerate(data):
                self.setData(self.index(i, 0, parent), str(d))
            self.layoutChanged.emit()
            return True
        except Exception as e:
            logger.error(str(e))
        return False

    def removeRowDataByFirst(self, first, parent=QtCore.QModelIndex()):
        self.layoutAboutToBeChanged.emit()
        for row in range(self.rowCount(parent)):
            if self.data(self.index(row, 0, parent)) == str(first):
                self.removeRow(row, parent)
                self.layoutChanged.emit()
                return True
        return False

    def clear(self, parent=QtCore.QModelIndex()):
        self.beginResetModel()
        self.removeRows(0, self.rowCount(parent), parent)
        self.endResetModel()

    def roleNames(self):
        d = dict()
        for i in range(self.rootItem.columnCount()):
            d[QtCore.Qt.UserRole + i] = self.rootItem.data(i).encode()
        return d

    def childCount(self, parent=QtCore.QModelIndex()):
        return self.getItem(parent).childCount()

    def getIndexFromData(self, data, parent=QtCore.QModelIndex()):
        if self.data(parent) == str(data):
            return parent
        else:
            for row in range(self.rowCount(parent)):
                index = self.getIndexFromData(str(data), self.index(row, 0, parent))
                if index:
                    return index
        return None

    def columnCount(self, parent=QtCore.QModelIndex()):
        if parent.isValid():
            return parent.internalPointer().columnCount()
        else:
            return self.rootItem.columnCount()

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return None
        item = index.internalPointer()
        if role == QtCore.Qt.DisplayRole:
            return item.data(index.column())
        elif role >= QtCore.Qt.UserRole:
            return item.data(role - QtCore.Qt.UserRole)

    def flags(self, index):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def getItem(self, index):
        if index.isValid():
            item = index.internalPointer()
            if item:
                return item

        return self.rootItem

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.rootItem.data(section)

    def index(self, row, column, parent=QtCore.QModelIndex()):
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def insertColumns(self, position, columns, parent=QtCore.QModelIndex()):
        self.beginInsertColumns(parent, position, position + len(columns) - 1)
        success = self.rootItem.insertColumns(position, len(columns))
        if success:
            for col in range(len(columns)):
                self.rootItem.setData(position+col, columns[col])
        self.endInsertColumns()

        return success

    def insertRows(self, position, rows, parent=QtCore.QModelIndex()):
        parentItem = self.getItem(parent)
        self.beginInsertRows(parent, position, position + rows - 1)
        success = parentItem.insertChildren(position, rows,
                self.rootItem.columnCount())
        self.endInsertRows()

        return success

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()

        childItem = index.internalPointer()
        parentItem = childItem.parent()

        if parentItem == self.rootItem:
            return QtCore.QModelIndex()

        return self.createIndex(parentItem.childNumber(), 0, parentItem)

    def removeColumns(self, position, columns, parent=QtCore.QModelIndex()):
        self.beginRemoveColumns(parent, position, position + columns - 1)
        success = self.rootItem.removeColumns(position, columns)
        self.endRemoveColumns()

        if self.rootItem.columnCount() == 0:
            self.removeRows(0, self.rowCount())

        return success

    def removeRows(self, position, rows, parent=QtCore.QModelIndex()):
        parentItem = self.getItem(parent)

        self.beginRemoveRows(parent, position, position + rows - 1)
        success = parentItem.removeChildren(position, rows)
        self.endRemoveRows()

        return success

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.column() > 0:
            return 0

        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()

        return parentItem.childCount()

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        if role != QtCore.Qt.EditRole:
            return False

        item = self.getItem(index)
        result = item.setData(index.column(), value)

        return result

    def setHeaderData(self, section, orientation, value, role=QtCore.Qt.EditRole):
        if role != QtCore.Qt.EditRole or orientation != QtCore.Qt.Horizontal:
            return False

        result = self.rootItem.setData(section, value)
        if result:
            self.headerDataChanged.emit(orientation, section, section)

        return result

    def setupModelData(self, lines, parent):
        parents = [parent]
        indentations = [0]

        number = 0

        while number < len(lines):
            position = 0
            while position < len(lines[number]):
                if lines[number][position] != '\t':
                    break
                position += 1

            lineData = lines[number][position:].strip()

            if lineData:
                # Read the column data from the rest of the line.
                columnData = [s for s in lineData.split("\t") if s]

                if position > indentations[-1]:
                    # The last child of the current index is now the new
                    # index unless the current index has no children.

                    if parents[-1].childCount() > 0:
                        parents.append(parents[-1].child(parents[-1].childCount() - 1))
                        indentations.append(position)

                else:
                    while position < indentations[-1] and len(parents) > 0:
                        parents.pop()
                        indentations.pop()

                # Append a new item to the current index's list of children.
                parents[-1].appendChild(TreeItem(columnData, parents[-1]))

            number += 1
